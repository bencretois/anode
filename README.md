**Please note** that the full methodology of the model training and the data collection will soon be released in a Arxiv pre-print and that **this repo may be subject to change**.

# ANODE: Anthropogenic noise detector

The aim of this project is to provide a model that can **identify human speech in an ecosystem**. Such models can be useful to to anonymize data or quantify the degree of sound pollution an environment is subject to.

Models providing support for Voice Active Detection (VAD) such as PyAnnote or Webrctvad are great for general purposes but tend to return false postitive (i.e. detects speech while there is no one speaking) when use on soundscape records. This is due to the various natural noises such as bird songs which can be interpreted as human speech.

Our model has been trained specifically on soundscape data and returns less false positive than other models when use on soundscape.

Our model uses a modified VGG11. We added dropout layers after the fully connected layers to avoid overfitting and added a batch normalization layer after each maxPool layers.


## How to use our model on your data

First of all, make sure **docker** is installed on your system. [This link](https://docs.docker.com/get-docker/) should get you started with docker installation. Then, clone the entire repository in a local repository and place your data in the folder `data2predict`. The data should be `.wav` files. `data2predict` can contains one or multiple `.wav` files.

Now you just have to open a shell and execute the following command:

`./docker_run_predictSpeech.sh`

This should return a `json file` in the folder output for each `.wav` file you have uploaded.

Examples have been provided in this repository.


--- 

## Things to improve

- Make a Web interface for dropping `.wav` files and not having to run any code
- Use a more complex model
- Include other anthropogenic noises

Please contact me and don't hesitate to let me know which features you would also like to be added!


