#!/bin/bash -x

docker run -it --rm  \
        -v $PWD:/app/ \
        registry.gitlab.com/bencretois/anode/main:cf7ca594627b2400e03c1b9802864abcf31df32b \
        poetry run python /app/predictSpeech.py \
             --input /app/data2predict/ \
             --output /app/output/ \
             --model_path /app/model_weights/ecoVAD_v1.0.pt \
             --threshold 0.8
