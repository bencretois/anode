#!/bin/bash -x

docker run \
    -p 8888:8888 \
    --rm -it \
    -v $PWD:/app \
    -v $HOME/.ssh:/root/.ssh:ro \
    -v $HOME/.gitconfig:/root/.gitconfig:ro \
    registry.gitlab.com/bencretois/anode/main:331a5f56a1d64b762a5df1e7e684dc195e5794d8 \
    poetry run jupyter lab --port=8888 --no-browser --ip=0.0.0.0 --allow-root
